local camera = Object:extend()

function camera:new(ww,wh)
  self.ww,self.wh = ww,wh
  self.x,self.y = 0,0
  self.vx,self.vy = 0,0
  self.scaleX,self.scaleY = 1,1
  self.rotation = 0
  self.speed = 0
  self.bw, self.bh = ww,wh
  self.cx, self.cy = ww/2,wh/2
end

function camera:set()
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
  love.graphics.translate(-self.x, -self.y)
  self.bw, self.bh = self.x+self.ww,self.y+self.wh
  self.cx, self.cy = self.x+self.ww/2, self.y+self.wh/2
end

function camera:unset()
  love.graphics.pop()
end

function camera:update(dt)
  self.speed = lerp(self.speed,0,dt)
  self.x = self.x + self.vx * self.speed
  self.y = self.y + self.vy * self.speed
end
function camera:move(dx, dy)
  self.x = self.x + (dx or 0)
  self.y = self.y + (dy or 0)
end

function camera:rotate(dr)
  self.rotation = self.rotation + dr
end

function camera:scale(sx, sy)
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function camera:setPosition(x, y)
  self.x = x or self.x
  self.y = y or self.y
end

function camera:setScale(sx, sy)
  self.scaleX = sx or self.scaleX
  self.scaleY = sy or self.scaleY
end

function camera:follow(x,y,deadzone)
  local angle = math.atan2(y - self.cy, x - self.cx)
  local distance = math.dist(x, y, self.cx, self.cy)
  if distance > deadzone then
    self.vx = math.cos(angle)
    self.vy = math.sin(angle)
    self.speed = distance/40
  end
end

return camera