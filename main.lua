require 'libs.math'
require 'libs.utils'
require 'enet'
require 'def'
Object  = require 'libs.classic'
Camera  = require 'libs.camera'
Net     = require 'net'
Bullets = require 'bullets'
Snake   = require 'snake'
Player  = require 'player'
Grid    = require 'grid'

love.window.setMode(ww,wh)
love.window.setTitle("SnakeFight")

function love.load()
    love.graphics.setBackgroundColor(background_color)
    camera = Camera(ww,wh)
    player = Player(2)
    grid   = Grid()
    net    = Net("192.168.0.102:6789","test")
end

function love.touchmoved( id, x, y, dx, dy, pressure )
    player:touchmoved(id, x, y, dx, dy, pressure)
end

function love.touchpressed(id, x, y, dx, dy, pressure)
    player:touchpressed(id, x, y, dx, dy, pressure)
end

function love.touchreleased(id, x, y, dx, dy, pressure)
    player:touchreleased(id, x, y, dx, dy, pressure)
end

function love.keypressed(key)
    player:keypressed(key)
end

function love.update(dt)
    player:update(dt)
    camera:update(dt)
    net:update(dt)
end

function love.draw()
    camera:set()
    grid:draw()
    player:draw()
    camera:unset()
end

function love.quit()
    net:destroy()
end