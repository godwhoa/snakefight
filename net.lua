local Net = Object:extend()

function Net:new(addr,channel)
	self.host = enet.host_create()
	self.server = self.host:connect(addr)
	self.channel = channel
end

function Net:send(msg)
	self.server:send(msg)
end

function Net:update(dt)
	local event = self.host:service(0)
	if event then
		if event.type == "connect" then
			event.peer:send(self.channel)
		elseif event.type == "receive" then
		else
		end
	end
end

function Net:destroy()
	self.server:disconnect()
	self.host:flush()
end

return Net