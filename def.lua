ww = 960
wh = 540

sw = 10*2
sh = 10*2

background_color = {197,233,155,255*0.7}
gridcolor = {255, 254, 89}
snake_color = {193,98,0}
bullet_color = {155, 81, 224}
clear_color = {255,255,255}


map_w = ww*15
map_h = wh*15

o_fac = 2
map_x = -ww*o_fac
map_y = -wh*o_fac

-- has one ww or wh padding
map_left = (map_x/o_fac+1)
map_right = map_w+(map_x*o_fac+1)
map_top = (map_y/o_fac+1)
map_bottom = map_h+(map_y*o_fac+1)

print(string.format("left: %d right: %d top: %d bottom: %d",map_left/sw,map_right/sw,map_top/sh,map_bottom/sh))

