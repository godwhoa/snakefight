require 'libs.math'

Bullets = Object:extend()

function Bullets:new(cb)
	self.w,self.h = sw-sw/2,sh-sh/2
	self.ox,self.oy = 2,2
	self.speed = 20
	self.timelimit = 5
	self.limit = 30
	self.bullets = {}
	self.cb = cb
end

function Bullets:add(x1,y1,dir)
	if #self.bullets < self.limit then
		local vx,vy = v_dir(dir)
		table.insert(self.bullets,{
			time = 0,
			x=x1,y=y1,
			vx=vx,vy=vy
		})
	end
end

function Bullets:remove(i)
	table.remove(self.bullets,i)
end

function Bullets:update(dt)

	for i,b in ipairs(self.bullets) do
		-- keep track of time
		b.time = b.time + dt
		-- callback for how we deal with collision
		if self.cb ~= nil then
			self.cb(i,b)
		end

		-- destroy if time expires
		if b.time >= self.timelimit then
			self:remove(i)
		end

		b.x = b.x + b.vx * dt * self.speed
		b.y = b.y + b.vy * dt * self.speed
	end
end

function Bullets:draw()
	love.graphics.setColor(bullet_color)
	for i,b in ipairs(self.bullets) do
		love.graphics.rectangle("fill", (b.x*sw)-self.w/2, (b.y*sh)-self.h/2, self.w, self.h )
	end
	love.graphics.setColor(clear_color)
end
return Bullets