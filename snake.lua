local Snake = Object:extend()

function Snake:new(len)
	-- for easy access
	self.x,self.y=0,0
	-- timestep
	self.step = 1/10
	self.time = 0
	-- direction
	self.dir = ""
	-- segments of snake {{x,y},...}
	self.seg = {{x=love.math.random(ww/sw),y=love.math.random(wh/sh)}}
	-- initial len.
	self:add(len)
	-- camera deadzone
	self.deadzone = ((ww+wh)/2)/8
	-- bullets
	self.bullets = Bullets()
	-- gun
	-- self.bw,self.bh = (sw-sw/2),(sh-sh/2)*4
end

function Snake:add(len)
	for i=1,len do
		local last = {x=self.seg[1].x,y=self.seg[1].y}
		-- it trickles down so we'll just clone the last segment
		table.insert(self.seg,last)
	end
end

function Snake:movement(dt)
	-- trickle down the movement head to tail.
	for i=#self.seg,2,-1 do
		self.seg[i].x = self.seg[i-1].x
		self.seg[i].y = self.seg[i-1].y
	end
	--direction
	if self.dir == "up" then
		self.seg[1].y = self.seg[1].y - 1
	end
	if self.dir == "down" then
		self.seg[1].y = self.seg[1].y + 1
	end
	if self.dir == "left" then
		self.seg[1].x = self.seg[1].x - 1
	end
	if self.dir == "right" then
		self.seg[1].x = self.seg[1].x + 1
	end
	self.x,self.y = math.floor(self.seg[1].x),math.floor(self.seg[1].y)
	--teleport if out of bounds
	-- left edge
	if self.seg[1].x < math.floor(map_left/sw) then
		print("left edge")
		self.seg[1].x = math.floor((map_right+map_x)/sw)
		camera.x = map_right+map_x
	end
	-- top edge
	if self.seg[1].y < math.floor(map_top/sh) then
		print("top edge")
		self.seg[1].y = math.floor((map_bottom+map_y)/sh)
		camera.y = map_top+map_y
	end
	-- right edge
	if self.seg[1].x > math.floor(map_right/sw) then
		print("right edge")
		self.seg[1].x = math.floor((map_left)/sw)
		camera.x = map_left
	end
	
	-- -- bottom edge
	if self.seg[1].y > math.floor(map_bottom/sh) then
		print("bottom edge")
		self.seg[1].y = math.floor((map_top)/sh)
		camera.x = map_top
	end
end

function Snake:shoot()
	self.bullets:add(self.x+(sw/2)/sw,self.y+(sh/2)/sh,self.dir)
end

function Snake:update(dt)
	self:update_other(dt)
	self.bullets:update(dt)
	self.time = self.time + dt
	if self.time >= self.step then
		self.time = self.time - self.step
		self:movement(dt)
	end
	snake_color[2] = (255*dt)

	camera:follow(self.x*sw,self.y*sh,self.deadzone)
end

function Snake:draw()
	self.bullets:draw()
	for i,s in ipairs(self.seg) do
		snake_color[4] = (255/i)
		love.graphics.setColor(snake_color)
		love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw, sh)
	end
	-- love.graphics.setColor(bullet_color)
	-- love.graphics.rectangle("fill", (self.x*sw)+self.bw/2, (self.y*sh)+self.bh/2, self.bw, self.bh )
	love.graphics.setColor(clear_color)
end

return Snake