local Player = Snake:extend()

function Player:new(len)
	Player.super.new(self,len)
	self.tap = 0
	self.taptimeout = 0.3
	self.swipedist = 15
end

function Player:keypressed(key)
	-- move
	if m_one(key,{'up','down','left','right'}) then
		self.dir = key
		net:send(self.dir)
	end
	-- shoot
	if key == "space" then
		self:shoot()
		net:send("shoot")
	end
end

function Player:touchpressed(id, x, y, dx, dy, pressure)
	if self.tap <= self.taptimeout then
		self:shoot()
		net:send("shoot")
	end
end

function Player:touchreleased(id, x, y, dx, dy, pressure)
	self.tap = 0
end

function Player:touchmoved(id, x, y, dx, dy, pressure)
	if math.abs(dx) > math.abs(dy) and math.abs(dx) >= self.swipedist then
		if dx > 0 then
			self.dir = "right"
		else
			self.dir = "left"
		end
	elseif math.abs(dy) >= self.swipedist then
		if dy > 0 then
			self.dir = "down"
		else
			self.dir = "up"
		end
	else
	end
	net:send(self.dir)
end

function Player:update_other(dt)
	self.tap = self.tap + dt
end

return Player