local Grid = Object:extend()

function Grid:new()
	self.canvas = love.graphics.newCanvas(sw, sh)
	self.canvas:setWrap("repeat","repeat")
	self.bg_quad = love.graphics.newQuad(0, 0, map_w, map_h, sw, sh)
	love.graphics.setCanvas(self.canvas)
	love.graphics.setColor(gridcolor)
	love.graphics.rectangle('line', 0, 0, sw, sh)
	love.graphics.setColor(clear_color)
	love.graphics.setCanvas()
end

function Grid:draw()
	love.graphics.draw(self.canvas,self.bg_quad, map_x, map_y)
end

return Grid